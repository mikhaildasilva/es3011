#include "simc.h"

byte controlMode = VELOCITY; // MOTOR CONTROL MODE
byte returnData[RETURN_VARS*RETURN_VAR_BYTES]; // RETURN DATA SENT TO THE CONTROLLER UPON REQUEST
double encoderCount = 0; // CURRENT ENCODER COUNT
double lastCount = 0;    // PREVIOUS ENCODER COUNT
long now = 0;              // TIME OF PREVIOUS ENCODER READING
long lastTime = 0;         // TIME OF PREVIOUS ENCODER READING
double velocity = 0.0;   // MOTOR SPEED IN RPM
double motorCurrent = 0.0; // MOTOR CURRENT (IN ADC)
double* returnVars[RETURN_VARS]={&encoderCount,&velocity,&motorCurrent}; // RETURN DATA VARIABLES

// PID
double targetPos = 0.0, targetVel = 0.0; // TARGET POSITION AND SPEED
double u = 0.0, dU = 0.0; // CONTROL EFFORT
// double* returnVars[RETURN_VARS]={&encoderCount,&velocity,&targetVel}; // RETURN DATA VARIABLES
PID pid[2] = {
  //PID(&encoderCount, &u, &targetPos, 0.5, 0.005, 0.005, REVERSE), // POSITION PID
  //PID(&velocity, &dU, &targetVel, 0.5, 0.005, 0.005, REVERSE) // VELOCITY PID
  PID(&encoderCount, &u, &targetPos, 0.5, 0.005, 0.005, DIRECT), // POSITION PID
  PID(&velocity, &dU, &targetVel, 0.5, 0.005, 0.005, DIRECT) // VELOCITY PID
};

void setup() {
  // INIT I2C W/ GIVEN ADDRESS
  Wire.begin(ADDRESS);

  // SET SENSOR PINS
  pinMode(CURRENT_SENSOR, INPUT);
  pinMode(ENCODER1, INPUT_PULLUP);
  pinMode(ENCODER2, INPUT_PULLUP);

  // SET OUTPUT PINS
  pinMode(NSLEEP, OUTPUT);
  pinMode(M1, OUTPUT);
  pinMode(M2, OUTPUT);

  // ATTACH INTERRUPTS FOR ENCODER READINGS
  attachInterrupt(digitalPinToInterrupt(ENCODER1), isr1, CHANGE);
  attachInterrupt(digitalPinToInterrupt(ENCODER2), isr2, CHANGE);

  // SET I2C CALLBACKS
  Wire.onRequest(onRequest);
  Wire.onReceive(onReceive);

  digitalWrite(NSLEEP, HIGH); //  NSLEEP SHOULD BE KEPT HIGH FOR NORMAL OPERATION OF THE MD9927 MOTOR DRIVER

  pid[POSITION].SetMode(AUTOMATIC);                                      // SET POSITION PID MODE
  pid[POSITION].SetOutputLimits((double)-MAX_PWM, (double)MAX_PWM); // CONSTRAIN POSITION PID OUTPUT
  pid[POSITION].SetSampleTime(PID_SAMPLE_TIME);                          // SET POSITION PID SAMPLING TIME

  pid[VELOCITY].SetMode(AUTOMATIC);                                      // SET VELOCITY PID MODE
  pid[VELOCITY].SetOutputLimits((double)-MAX_PWM, (double)MAX_PWM); // CONSTRAIN VELOCITY PID OUTPUT
  pid[VELOCITY].SetSampleTime(PID_SAMPLE_TIME);                          // SET VELOCITY PID SAMPLING TIME
}

void loop() {
  now = millis();
  double dt = now - lastTime;

  // CALCULATE EFFORT, READ CURRENT, AND CALCULATE SPEED EVERY 10 MS
  if (dt >= PID_SAMPLE_TIME) {
    // UPDATE SPEED AND CURRENT
    calcVelocity(dt);
    readCurrent();
    
    // SET MOTOR EFFORT
    pid[controlMode].Compute();
    if (controlMode == VELOCITY) {
      u = constrain(u + dU, -MAX_PWM, MAX_PWM);
    }
    u = round(u);
    drive(u);
    
    lastCount = encoderCount;
    lastTime = now; // UPDATE TIMER
  }
}

// ISRs split up for each hall-effect latch sensor to decrease processing time within the ISR and allow I2C to work every time without interruption

/**
   ISR for encoder 1 - reads registers instead of digital read for increased speed and increments/decrements a encoderCount variable
*/
void isr1() {
  byte encread1 = PIND >> 2 & 0x01;
  byte encread2 = PIND >> 3 & 0x01; // GET THE TWO BIT ENCODER READ
  if (encread1 == encread2) encoderCount++;
  else encoderCount--;
}

/**
   ISR for encoder 2 - reads registers instead of digital read for increased speed and increments/decrements a encoderCount variable
*/
void isr2() {
  byte encread1 = PIND >> 2 & 0x01;
  byte encread2 = PIND >> 3 & 0x01; // GET THE TWO BIT ENCODER READ
  if (encread1 != encread2) encoderCount++;
  else encoderCount--;
}

/**
   Function to set the motorCurrent variable to the current sensor reading and check if it is stalled
*/
void readCurrent() {
  motorCurrent = (double)analogRead(CURRENT_SENSOR);
}

void calcVelocity(double dt) {
  double countDiff = encoderCount - lastCount;                   // CHANGE IN ENCODER ENCODERCOUNT
  velocity = (countDiff / dt) * ENCODER_TICKS_PER_MS_TO_RPM; // CALCULATE MOTOR RPM
}

void drive(int16_t pwm) {
  byte pin1, pin2;

  if (pwm > 0) { // FORWARD
    pin1=0;
    pin2=pwm;
  } else if (pwm < 0) { // REVERSE
    pin1=-pwm;
    pin2=0;
  } else { // BRAKE
    pin1=MAX_PWM;
    pin2=MAX_PWM;
  }

  analogWrite(M1, pin1);
  analogWrite(M2, pin2);
}

double decodeDouble(byte* bytes, int startIndex) {
  int32_t coeff = (((int32_t)bytes[startIndex] << 24) | ((int32_t)bytes[startIndex+1] << 16) | ((int32_t)bytes[startIndex+2] << 8) | (int32_t)bytes[startIndex+3]); // DECODE 32-BIT COEFFICIENT
  byte exp = (byte)bytes[startIndex+4]; // DECODE 8-BIT EXPONENT
  return coeff*pow(10,-exp);
}

encoded_double encodeDouble(double val) {
  encoded_double encodedVal;

  while (fmod(val,1.0) > 0.0) {
    val *= 10;
    encodedVal.exp++;
  }

  int32_t intVal=(int32_t)val;
  encodedVal.coeff[0] = intVal >> 24;
  encodedVal.coeff[1] = intVal >> 16;
  encodedVal.coeff[2] = intVal >> 8;
  encodedVal.coeff[3] = intVal & 0xFF;

  return encodedVal;
}

void tunePID(int pidType, byte *args, int argNum) {
  double K[3];
  for (int i=0; i < 3; i++) {
    // DECODE DOUBLE
    K[i]=decodeDouble(args,5*i); 
  }

  pid[pidType].SetTunings(K[0],K[1],K[2]);
}

void _tunePosPID(byte *args, int argNum) {
  tunePID(POSITION,args,argNum);
}

void _tuneVelPID(byte *args, int argNum) {
  tunePID(VELOCITY,args,argNum);
}

void _setPosition(byte *args, int argNum) {
  controlMode=POSITION; // SET CONTROL MODE

  // SET THE TARGET POSITION
  targetPos = decodeDouble(args,0);
}

void _setVelocity(byte *args, int argNum) {
  controlMode=VELOCITY; // SET CONTROL MODE

  // SET THE TARGET VELOCITY
  targetVel = decodeDouble(args,0);
}

void _reset(byte *args, int argNum) {
  drive(0); // STOP MOTOR

  // RESET DRIVER VARIABLES
  controlMode=VELOCITY;
  encoderCount=0;
  lastCount = 0;
  now = 0;
  lastTime = 0;
  velocity = 0.0;
  targetPos=0.0;
  targetVel=0.0;
  u = 0.0;
  dU = 0.0;
}

/**
   Callback function upon receiving a request for returnData via I2C from master
   This will request a set number of bytes as a message that will be formed for interpretation by the mainboard
*/
void onRequest() {
  for (int i=0; i < RETURN_VARS; i++) {
    encoded_double encVal=encodeDouble(*returnVars[i]); // ENCODE VARIABLE
    returnData[5*i] = encVal.coeff[0]; // PLACE ENCODED DATA INTO RETURN DATA ARRAY
    returnData[5*i+1] = encVal.coeff[1];
    returnData[5*i+2] = encVal.coeff[2];
    returnData[5*i+3] = encVal.coeff[3];
    returnData[5*i+4] = encVal.exp;
  }

  Wire.write(returnData, RETURN_VARS*RETURN_VAR_BYTES); // SEND DATA
}

/**
   callback function for recieving messages and setting the appropriate I2C status
*/
void onReceive(int numBytes) {
  byte funcIndex = Wire.read(); // SET MOTOR DRIVER FUNCTION

  // READ ARGS
  int argNum=numBytes-1;
  byte args[max(argNum,1)]={0}; // INIT ARGS ARRAY W/ AT LEAST 1 VALUE
  int i = 0;
  while (Wire.available() > 0) args[i++] = Wire.read();
  funcMap[funcIndex](args,argNum); // EXECUTE FUNCTION
}