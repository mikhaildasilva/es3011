#include <Arduino.h>
#include <Wire.h>
#include <math.h>
#include <PID_v1.h>

#define ADDRESS 0x07 // SMART MOTOR DRIVER I2C ADDRESS
#define NSLEEP 5
#define M1 9 // MOTOR PINS
#define M2 10
#define CURRENT_SENSOR 17 // CURRENT SENSOR PIN
#define ENCODER1 2        // ENCODER PINS
#define ENCODER2 3

#define PID_SAMPLE_TIME 10 // PID SAMPLE TIME (IN MS)

// MOTOR PROPERTIES
#define MAX_PWM 255              // MAXIMUM PWM VALUE
#define GEAR_RATIO 150           // MOTOR GEAR RATIO
#define ENCODER_TICKS_PER_REV 12 // NO OF HIGH PULSES PER ROTATION
#define ENCODER_TICKS_PER_MS_TO_RPM (1000.0 * 60.0) / (double)(ENCODER_TICKS_PER_REV * GEAR_RATIO) // MULTIPLIER TO CONVERT FROM ENCODER TICKS PER MS TO RPM

#define RETURN_VAR_BYTES 5 // NUMBER OF RETURN DATA BYTES
#define RETURN_VARS 3 // NUMBER OF RETURN DATA BYTES

// MOTOR DRIVER CONTROL MODES
#define POSITION 0
#define VELOCITY 1

// MOTOR DRIVER FUNCTIONS
#define TUNE_POSITION_PID 0
#define TUNE_VELOCITY_PID 1
#define SET_POSITION 2
#define SET_VELOCITY 3
#define RESET 4
void _tunePosPID(byte *args, int argNum);
void _tuneVelPID(byte *args, int argNum);
void _setPosition(byte *args, int argNum);
void _setVelocity(byte *args, int argNum);
void _reset(byte *args, int argNum);

// FUNCTION MAP
void (*funcMap[])(byte *args, int argNum) = {_tunePosPID,_tuneVelPID,_setPosition,_setVelocity,_reset};

struct encoded_double {
  byte coeff[4]={0};
  byte exp=0;
};