#ifndef PID_v1_h
  #define PID_v1_h
  #define LIBRARY_VERSION	1.0.0

  #include "Arduino.h"

  class PID {
    public:
      // OPERATING MODES
      #define AUTOMATIC	1
      #define MANUAL	0

      // CONTROL DIRECTIONS
      #define DIRECT  0
      #define REVERSE  1

      /**
       * SETS THE INITIAL TUNING PARAMETERS AND LINKS THE PID TO THE INPUT, OUTPUT, AND SETPOINT
      */
      PID(double*, double*, double*, double, double, double, int);
      
      /**
       * SETS THE OPERATING MODE TO EITHER MANUAL (0) OR AUTO (NON-0)
      */
      void setMode(int mode);

      /**
       * COMPUTES CONTROL EFFORT WHICH SHOULD BE CALLED EVERY TIME LOOP() CYCLES
      */
      void compute();

      /**
       * CONSTRAINS THE OUTPUT TO A GIVEN RANGE. 0-255 BY DEFAULT
      */
      void setOutputConstraints(double, double);
      
      /**
       * SETS TUNING PARAMETERS
      */
      void setTuningParams(double, double, double);         	

      /**
       * SETS THE DIRECTION OF THE CONTROLLER. DIRECT CAUSES THE OUTPUT TO INCREASE WHEN THE ERROR IS POSITIVE
       * WHILE REVERSE RESULTS IN THE OPPOSITE BEHAVIOR
      */
      void setDirection(int);

      /**
       * SETS THE COMPUTATION FREQUENCY IN MILLISECONDS
      */
      void setSampleTime(int);
      
      // DISPLAY FUNCTIONS
      double getKp();		
      double getKi();		
      double getKd();		
      int getMode();		
      int getDirection();

    private:
      void _initialize();
      
      // DISPLAY VARIABLES
      double _dispKp;
      double _dispKi;
      double _dispKd;
        
      double _kp; // (P)ROPORTIONAL TUNING PARAMETER
      double _ki; // (I)NTEGRAL TUNING PARAMETER
      double _kd; // (D)ERIVATIVE TUNING PARAMETER

      // POINTERS TO THE INPUT, OUTPUT, AND SETPOINT VARIABLES
      double *_input;              
      double *_output;  
      double *_setpoint;
            
      int _controlDirection;
      unsigned long _lastTime;
      double _ITerm, _lastInput;

      int _sampleTime;
      double _outMin, _outMax;
      bool _inAuto;
  };
#endif