/**********************************************************************************************
 * Arduino PID Library - Version 1
 * by Brett Beauregard <br3ttb@gmail.com> brettbeauregard.com
 *
 * This Code is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 **********************************************************************************************/

#include "Arduino.h"
#include "PID_v1.h"

PID::PID(double* input, double* output, double* setpoint,
        double Kp, double Ki, double Kd, int direction) {
	PID::setOutputConstraints(0,255); // DEFAULT OUTPUT LIMIT CORRESPONDS TO THE ARDUINO PWM LIMITS
   
   _sampleTime = 100; // DEFAULT CONTROLLER SAMPLE TIME IS 0.1 SECONDS
   
   PID::setDirection(direction);
   PID::setTuningParams(Kp,Ki,Kd);

   _lastTime = millis() - _sampleTime;				
   _inAuto = false; // DEFAULT TO MANUAL OPERATING MODE

   // SET INPUT, OUTPUT, AND SETPOINT LINKS
   _input = input;
   _output = output;
   _setpoint = setpoint;
}

void PID::compute() {
   if(!_inAuto) return;

   unsigned long now = millis();
   int timeChange = (now - _lastTime);
   if(timeChange >= _sampleTime) {
      // COMPUTE ALL THE WORKING ERROR VARIABLES
	   double input = *_input;
      double error = *_setpoint - input;
      _ITerm+= (_ki * error);
      if(_ITerm > _outMax) _ITerm= _outMax;
      else if(_ITerm < _outMin) _ITerm= _outMin;
      double dInput = (input - _lastInput);
 
      // COMPUTE PID OUTPUT
      double output = _kp * error + _ITerm - _kd * dInput;
      *_output = constrain(output,_outMin,_outMax);
	  
      // REMEMBER SOME VARIABLES FOR NEXT TIME
      _lastInput = input;
      _lastTime = now;
   }
}

void PID::setTuningParams(double Kp, double Ki, double Kd) {
   if (Kp<0 || Ki<0 || Kd<0) return;
 
   _dispKp = Kp; _dispKi = Ki; _dispKd = Kd;
   
   double sampleTimeInSec = ((double)_sampleTime) / 1000;  
   _kp = Kp;
   _ki = Ki * sampleTimeInSec;
   _kd = Kd / sampleTimeInSec;
 
   if(_controlDirection == REVERSE) {
      _kp = (0 - _kp);
      _ki = (0 - _ki);
      _kd = (0 - _kd);
   }
}

void PID::setSampleTime(int newSampleTime) {
   if (newSampleTime > 0) {
      double ratio = (double)newSampleTime / (double)_sampleTime;
      _ki *= ratio;
      _kd /= ratio;
      _sampleTime = (unsigned long)newSampleTime;
   }
}

void PID::setOutputConstraints(double min, double max) {
   if(min >= max) return;
   _outMin = min;
   _outMax = max;
 
   if(_inAuto) {
	   *_output = constrain(*_output,_outMin,_outMax);
	  _ITerm = constrain(_ITerm,_outMin,_outMax);
   }
}

void PID::setMode(int mode) {
   bool newAuto = (mode == AUTOMATIC);
   if(newAuto == !_inAuto) PID::_initialize();
   _inAuto = newAuto;
}

void PID::_initialize() {
   _ITerm = *_output;
   _lastInput = *_input;
   _ITerm = constrain(_ITerm,_outMin,_outMax);
}

void PID::setDirection(int direction) {
   if(_inAuto && (direction != _controlDirection)) {
      _kp = (0 - _kp);
      _ki = (0 - _ki);
      _kd = (0 - _kd);
   }   
   _controlDirection = direction;
}

double PID::getKp(){ return  _dispKp; }
double PID::getKi(){ return  _dispKi; }
double PID::getKd(){ return  _dispKd; }
int PID::getMode(){ return  _inAuto ? AUTOMATIC : MANUAL; }
int PID::getDirection(){ return _controlDirection;}