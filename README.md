# ES3011

[[_TOC_]]

## Triangular Velocity Profile

### Calculation of DISTANCE covered by motor

#### Distance covered in 1 cycle of the velocity profile

$\nu$ = $\frac{RPM_{max}}{2} = \frac{115}{2}$ = 57.5 RPM

(You can find the max RPM from the datasheet)

<details>
<summary> Datasheet Image</summary>
<br>
<img src="Images/pololu-motor-datasheet.PNG" alt="Datasheet">
</details>

#### Distance covered during Ramp-Up phase ($D_{up}$)

$D_{up}$ = $\nu_{avg} \times T \times \frac{C}{60}$

Total Distance covered would be ramp-up and ramp-down.

$D_{TOTAL}$ = $2 \times D_{up}$

#### Total distance covered

If $\Delta vel$ is repeated $n$ times

$D_{covered}$ = $n \times D_{TOTAL}$
